#!/usr/bin/python3

#============================================================
# Скрипт тестирования пиконов выполняет следующее:
# 1. перемещает ненужные пиконы в other_dir
# 2. удаляет из dir4move все что уже есть в picons_dir
# 3. проверяет, есть ли в other_dir файлы, у которых имя совпадает с файлом из picons_dir
# 4. генерирует список пиконов, которых не хватает в файл fname_need
#------------------------------------------------------------
# https://gitlab.com/it999/picons - репозиторий проекта
#============================================================

# эталонный список каналов
fname_ch_list = "ILookTV_list.md"

# список пиконов, которых не хватает
fname_need = "ILookTV_need.md"

# название папки с пиконамаи
picons_dir = "./black"

# название папки для пиконов, которые не нужны
other_dir = "./black_other"
#------------------------------------------------------------

import os, shutil, hashlib

# Конвертация имени канала в название пикона
def chname_to_picon_name(chname):
    fname = chname
    # Спецсимволы в названии канала заменяю на пробелы
    fname = fname.replace('/', ' ')
    fname = fname.replace(':', ' ')
    fname = fname.replace('|', ' ')
    fname += ".png"
    return fname

# Конвертация markdown списка каналов в список пиконов
def md_to_picon_list(md_file):
    ch_list = []
    f = open(fname_ch_list)
    for line in f:
        line = line.strip()
        prefix = line[:2]
        if prefix == "* ":
            fname = chname_to_picon_name(line[2:])
            ch_list.append(fname)
    return ch_list

# Перемещение из picons_dir в dir4move пиконов, которых нет в списке каналов fname_ch_list
def move_to_other(fname_ch_list, picons_dir, dir4move):
    # Получаю список пиконов из папки picons_dir
    picons = os.listdir(picons_dir)

    # Считываю список каналов из файла fname_ch_list
    ch_list = md_to_picon_list(fname_ch_list)

    # Перемещаю из picons_dir в dir4move пиконы, которых нет в списке fname_ch_list
    not_need = list(set(picons)-set(ch_list))
    for fname in not_need:
        print("Move %s to %s" % (fname, dir4move))
        shutil.move(picons_dir+"/"+fname, dir4move+"/"+fname)

# Удаление дубликатов пиконов из папки check_dir (сравниваю с main_dir)
def del_duplicate(check_dir, main_dir):
    picons = os.listdir(main_dir)
    hash_list  = {hashlib.md5(open(main_dir+"/"+fname, 'rb').read()).digest() for fname in picons}
    picons_alt = os.listdir(check_dir)
    for fname in picons_alt:
        fname_full = check_dir+"/"+fname
        fhash = hashlib.md5(open(fname_full, 'rb').read()).digest()
        if fhash in hash_list:
            print("Delete %s" % fname_full)
            os.remove(fname_full)
        elif fname in picons:
            print("Duplicate %s" % fname)
            os.remove(fname_full)

def gen_need(fname_ch_list, picons_dir, fname_need):
    f = open(fname_ch_list)
    fneed = open(fname_need, "w")
    is_begin = True
    for line in f:
        line = line.strip()
        prefix = line[:2]
        if prefix == "# ":
            group = line
            is_void = True
        if prefix == "* ":
            fname = chname_to_picon_name(line[2:])
            if not os.path.isfile(picons_dir+'/'+fname):
                if is_void:
                    if not is_begin:
                        fneed.write('\n')
                    fneed.write(group+'\n')
                    is_void = False
                    is_begin = False
                fneed.write(line+'\n')
    f.close()
    fneed.close()

#------------------------------------------------------------
print('=== Move to %s' % other_dir)
move_to_other(fname_ch_list, picons_dir, other_dir)

print('=== Delete duplicate in %s' % other_dir)
del_duplicate(other_dir, picons_dir)

print('=== Generate %s' % fname_need)
gen_need(fname_ch_list, picons_dir, fname_need)
