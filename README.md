Набор пиконов для IPTV
======================

Название файлов в точности соответствует названию каналов в плейлистаx [**ILook.TV**](https://ilook.tv/welcome/signup/9470a3204c056cfe) и **EdemTV**, есть пиконы и для других каналов.

[**Скачать готовый архив с пиконами**](https://epg.it999.ru/it999_dark_logo.zip)

[**Информация про настройку плейлистов**](https://epg.it999.ru/setup-playlist/)

## Есть пиконы, хочу поделиться. Как это сделать?
1. Желательно, чтобы пикон соответствовал шаблону (информация [в папке templates](https://gitlab.com/it999/picons/-/tree/master/templates))
2. Назовите пикон в точности как в плейлисте (см. [ILookTV_need.md](https://gitlab.com/it999/picons/-/blob/master/ILookTV_need.md) или [ILookTV_list.md](https://gitlab.com/it999/picons/-/blob/master/ILookTV_list.md))
3. Отправьте ваши пиконы любым из удобных способов:
   - [через git](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) - это способ для тех кто знает что такое git
   - создать на GitLab [Новый Issue/Обсуждение](https://gitlab.com/it999/picons/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) и прикрепить к нему архив с вашими пиконами
   - послать ссылку на архив с пиконами через [форму обратной связи на сайте it999.ru](https://epg.it999.ru/feedback/)

## Описание структуры папок
- **black** - папка с квадратными пиконами с темной подложкой для ILookTV
- **black_other** - папка с квадратными пиконами с темной подложкой для других каналов
- **templates** - папка с шаблоном и инструкцией по самостоятельному созданию пикона
- ILookTV_list.md - полный список каналов (группы, названия и их порядок в точности как оригинальном плейлисте). Обновляется ежедневно.
- ILookTV_need.md - пиконы, которых не хватает. Создается автоматически после добавления новых пиконов или после изменения плейлиста.
- test_picons.py - скрипт тестирования (осписание см. внутри скипта)

